#include <stdio.h>

const char BORDER = 'X';
const char EMPTY  = ' ';

/**
 * Draw square
 *
 * Example:
 *
 * XXXXXXXX
 * X      X
 * X      X
 * X      X
 * X      X
 * X      X
 * X      X
 * XXXXXXXX
 *
 * @param int size
 */
void drawSquare(int size)
{
    for (int y = 1; y <= size; y++) {
        printf("\n");
        for (int x = 1; x <= size; x++) {
            printf("%c", x == 1 || y == 1 || x == size || y == size ? BORDER : EMPTY);
        }
    }
}

/**
 * Draw straight triangle
 *
 * Example:
 *
 * X
 * XX
 * X X
 * X  X
 * X   X
 * X    X
 * X     X
 * XXXXXXXX
 *
 * @param size
 */
void drawStraightTriangle(int size)
{
    for (int y = 1; y <= size; y++) {
        printf("\n");
        for (int x = 1; x <= size; x++) {
            printf("%c", x == 1 || y == size || x == y ? BORDER : EMPTY);
        }
    }
}

/**
 * Draw rhombus
 *
 * Example:
 *
 *    XX
 *   X  X
 *  X    X
 * X      X
 * X      X
 *  X    X
 *   X  X
 *    XX
 *
 * @param size
 */
void drawRhombus(int size)
{
    int half = size / 2 + (size % 2);
    int offset = (size % 2);            //Offset for not even size

    for (int y = 1; y <= size; y++) {
        printf("\n");
        for (int x = 1; x <= size; x++) {
            _Bool isBorder =
                (x >= half && y <= half && x - half + offset == y) ||                           // Top-right main diagonal
                (x <= half && y >= half && x - offset == y - half) ||                           // Bottom-left main diagonal
                (x <= half && y <= half && x - 1 == half - y)      ||                           // Top-left side diagonal
                (x >= half && y >= half && x - half + offset == half - y + half + 1 - offset);  // Bottom-right side diagonal
            printf("%c", isBorder ? BORDER : EMPTY);
        }
    }
}

/**
 * Draw top triangle
 *
 * Example:
 *
 * XXXXXXXX
 *  X    X
 *   X  X
 *    XX
 *
 * @param size
 */
void drawTopTriangle(int size)
{
    int half = size / 2 + (size % 2);
    int even = 1 - (size % 2);          //Offset for even size

    for (int y = 1; y <= size; y++) {
        printf("\n");
        for (int x = 1; x <= size; x++) {
            _Bool isBorder =
                (x >= half && y <= half && x - half - even == half - y) || // Top-right side diagonal
                (x <= half && y <= half && x == y);                        // Top-left main diagonal
            printf("%c", y == 1 || isBorder ? BORDER : EMPTY);
        }
    }
}

int main()
{
    int size;

    scanf("%d", &size);

    if (size < 0) {
        printf("Size must be more zero");
        return 0;
    }

    drawSquare(size);

    printf("\n");

    drawStraightTriangle(size);

    printf("\n");

    drawRhombus(size);

    printf("\n");

    drawTopTriangle(size);

    return 0;
}